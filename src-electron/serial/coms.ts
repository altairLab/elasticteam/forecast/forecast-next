import { ForecastSerial, SerialPacket, listDevices } from './serial';
import { PacketID } from './enums';
import { mainWindow } from '../electron-main';
import { log_manager } from '../logger/manager'
import { ipcMain } from 'electron';

export const serial = new ForecastSerial();

ipcMain.handle('serial:list_devices', (event) => {
  return listDevices().then((res) => res.map(port => {
    if (port.path)
      return port.path;
  }));
});

serial.onData = (packet: SerialPacket) => {
  handlePacket(packet);
}

serial.onOpen = () => {
  mainWindow?.webContents.send('serial:open');
}

serial.onClose = () => {
  mainWindow?.webContents.send('serial:close');
}

ipcMain.handle('serial:is_connected', (event) => {
  return serial.isOpen;
});

ipcMain.handle('serial:connect', (event, port: string) => {
  if (!serial.isOpen)
    serial.open(port);
});

ipcMain.handle('serial:disconnect', (event) => {
  if (serial.isOpen)
    serial.close();
});

// PACKETS ---------------------------------------------------------------------

const serialWrite = (id: number, payload: Buffer) => {
  console.log(`Writing packet with id: ${id} and payload: ${payload.toString()}`)
  serial.write(id, payload);
}

const handlePacket = (packet: SerialPacket) => {
  const ack = packet.id;

  if(!packet.checksum)  {
    console.log("Checksum error");
    return;
  }

  try {
    switch (ack) {
      case PacketID.LOG:
          handleLog(packet.payload);
        break;
      case PacketID.ERROR:
        handleError(packet.payload);
        break;
      case PacketID.END_EXP:
        handleEndExperiment();
        break;
      case PacketID.ACK:
        const id = packet.payload.readUInt8(0);
        const payload = packet.payload.slice(1);
        parsePacket(id, payload);
        break;
      case PacketID.NACK:
        //console.log('Received NACK: ', packet.payload.slice(1).toString());
        handleError(packet.payload);
        break;
      default:
        console.log(`Error while handling incoming packet: ID (0x${packet.payload.readUInt8(0).toString(16)}) is neither ACK nor NACK`);
    }
  } catch (error) {
    console.log('Error: malformed packet in handlePAcket %s', error);
  }
};

const parsePacket = (id: number, payload: Buffer) => {
  switch (id) {
    case PacketID.LOG:
      handleLog(payload);
      break;
    case PacketID.ERROR:
      handleError(payload);
      break;
    case PacketID.HW_INFO:
      handleHWInfo(payload);
      break;
    case PacketID.VER:
      handleFWVersion(payload);
      break;
    case PacketID.CTRL_LIST:
      handleControllerList(payload);
      break;
    case PacketID.REF_LIST:
      handleReferencesList(payload);
      break;
    case PacketID.CTRL_PARAMS:
      handleControllerParameters(payload);
      break;
    case PacketID.REF_PARAMS:
      handleReferenceParameters(payload);
      break;
    case PacketID.CTRL_LOGS:
      handleControllerLoggables(payload);
      break;
    case PacketID.READY:
      handleReadyCheck(payload);
      break;
    default:
      console.log('ID? ', id.toString(16), payload.toString('utf8'));
  }
};

// ----- callbacks on message receive

const handleLog = (payload: Buffer) => {
  const result = Array<number>();
  const numberOfFloats = payload.length / 4;
  for (let i = 0; i < numberOfFloats; ++i) {
    result.push(payload.readFloatLE(i*4));
  }
  log_manager?.push(result);
}

const handleError = (payload: Buffer) => {
  const errorString = payload.toString('utf8');
  console.error('Received ERROR/NACK packet: ' + errorString)
  mainWindow?.webContents.send('serial:rcv_error', errorString);
}

const handleEndExperiment = () => {
  console.log('Packet received: END EXPERIMENT');
  mainWindow?.webContents.send('serial:rcv_end_exp');
}

const handleHWInfo = (payload: Buffer) => {
  console.log('Received HWInfo packet');

  const num_motors = payload.readUInt8(0);
  let hw_list: string[] = [];

  if (payload.length > 1) {
    hw_list = payload.slice(1).toString('utf8').split(',');
  }
  mainWindow?.webContents.send('ws:rcv_hw_info', num_motors, hw_list);
};

const handleFWVersion = (payload: Buffer) => {
  console.log('Received FW version packet');

  const version = payload.toString('utf8');
  mainWindow?.webContents.send('ws:rcv_fw_version', version);
};

const handleControllerList = (payload: Buffer) => {
  console.log('Received controllers list packet');

  const ctrl_list = payload.toString('utf8').split(',');
  mainWindow?.webContents.send('ws:rcv_ctrl_list', ctrl_list);
};

const handleReferencesList = (payload: Buffer) => {
  console.log('Received references list packet');

  const refs_list = payload.toString('utf8').split(',');
  mainWindow?.webContents.send('ws:rcv_refs_list', refs_list);
};

const handleControllerParameters = (payload: Buffer) => {
  console.log('Received controller parameters packet');

  const params_list = payload.toString('utf8').split(',');
  const controllerName = params_list[0];
  const parameters = params_list.slice(1)
  mainWindow?.webContents.send('ws:rcv_ctrl_params_'+ controllerName, controllerName, parameters);
};

const handleReferenceParameters = (payload: Buffer) => {
  console.log('Received reference parameters packet');

  const params_list = payload.toString('utf8').split(',');
  mainWindow?.webContents.send('ws:rcv_ref_params_' + params_list[0], params_list[0], params_list.slice(1));
};

const handleControllerLoggables = (payload: Buffer) => {
  console.log('Received controller loggables packet');

  const logs_list = payload.toString('utf8').split(',');
  const controllerName = logs_list[0];
  const loggables = logs_list.slice(1);
  mainWindow?.webContents.send('ws:rcv_ctrl_logs_' + controllerName, controllerName, loggables);
};

const handleReadyCheck = (payload: Buffer) => {
  const ready = payload.readUInt8(0);
  mainWindow?.webContents.send('ws:rcv_ready', Boolean(ready));
};


// ---- serial writes

const EOF = Buffer.from([0x00]); // All strings must be null terminated

export const getHWInfo = () => {
  serialWrite(PacketID.HW_INFO, Buffer.from([]));
};

export const getFWVersion = () => {
  serialWrite(PacketID.VER, Buffer.from([]));
};

export const getControllersList = () => {
  serialWrite(PacketID.CTRL_LIST, Buffer.from([]));
};

export const getReferencesList = () => {
  serialWrite(PacketID.REF_LIST, Buffer.from([]));
};

export const getControllerParams = (ctrl_id: string) => {
  const payload = Buffer.concat([Buffer.from(ctrl_id), EOF]);
  serialWrite(PacketID.CTRL_PARAMS, payload);
};

export const getReferenceParams = (ref_id: string) => {
  const payload = Buffer.concat([Buffer.from(ref_id), EOF]);
  serialWrite(PacketID.REF_PARAMS, payload);
};

export const getControllerLogs = (ctrl_id: string) => {
  const payload = Buffer.concat([Buffer.from(ctrl_id), EOF]);
  serialWrite(PacketID.CTRL_LOGS, payload);
};

export const getReady = () => {
  serialWrite(PacketID.READY, Buffer.from([]));
};

export const setController = (idController: string, num_motors: number, parameters: number[]) => {
  const idBuffer = Buffer.concat([Buffer.from(idController, 'utf8'), EOF]);
  const motorBuffer = Buffer.allocUnsafe(1);
  motorBuffer.writeUInt8(num_motors, 0);
  const parametersBuffer = Buffer.allocUnsafe(parameters.length * 4);
  parameters.forEach((value, index) => {
    parametersBuffer.writeFloatLE(value, index * 4);
  });
  const payload = Buffer.concat([idBuffer, motorBuffer, parametersBuffer]);
  serialWrite(PacketID.CTRL_SET, payload);
};

export const setHWLoggables = (loggablesHW: string[]) => {
  const hwBuffer = Buffer.from(loggablesHW.join(','), 'utf8');
  const payload = Buffer.concat([hwBuffer, EOF]);
  serialWrite(PacketID.HW_LOG_SET, payload);
};

export const setControllerLoggables = (motorNumber: number, loggablesControllers: string[]) => {
  const motorBuffer = Buffer.allocUnsafe(1);
  motorBuffer.writeUInt8(motorNumber, 0);
  const controllersBuffer = Buffer.from(loggablesControllers.join(','), 'utf8');
  const payload = Buffer.concat([motorBuffer, controllersBuffer, EOF]);
  serialWrite(PacketID.CTRL_LOG_SET, payload);
};

export const setReference = (id_ref: string, motor: number, parameters: number[]) => {
  const idBuffer = Buffer.concat([Buffer.from(id_ref, 'utf8'), EOF]);
  const motorBuffer = Buffer.allocUnsafe(1);
  motorBuffer.writeUInt8(motor, 0);
  const parametersBuffer = Buffer.allocUnsafe(parameters.length * 4);
  parameters.forEach((value, index) => {
    parametersBuffer.writeFloatLE(value, index * 4);
  });
  const payload = Buffer.concat([idBuffer, motorBuffer, parametersBuffer]);
  serialWrite(PacketID.REF_SET, payload);
};

export const startLoop = (frequency: number, duration: number) => {
  const payload = Buffer.allocUnsafe(8);
  console.log(frequency, duration)
  payload.writeFloatLE(frequency, 0);
  payload.writeFloatLE(duration, 4);
  serialWrite(PacketID.START_LOOP, payload);
};

export const standby = () => {
  serialWrite(PacketID.STANDBY, Buffer.from([]));
};

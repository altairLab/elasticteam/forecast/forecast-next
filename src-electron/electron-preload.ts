/**
 * This file is used specifically for security reasons.
 * Here you can access Nodejs stuff and inject functionality into
 * the renderer thread (accessible there through the "window" object)
 *
 * WARNING!
 * If you import anything from node_modules, then make sure that the package is specified
 * in package.json > dependencies and NOT in devDependencies
 *
 * Example (injects window.myAPI.doAThing() into renderer thread):
 *
 *   import { contextBridge } from 'electron'
 *
 *   contextBridge.exposeInMainWorld('myAPI', {
 *     doAThing: () => {}
 *   })
 */

import { contextBridge, ipcRenderer } from 'electron';
import { BrowserWindow } from '@electron/remote';
import { TaskFile } from 'src/models/files';
import { ExperimentOptions, CSVOptions, ExperimentDir } from 'src/models/logs'

declare global {
  interface Window {
    myWindowAPI: {
      minimize(): void;
      toggleMaximize(): void;
      close(): void;
    };
    openWindow: {
      plot(): Promise<void>;
    };
    fs_api: {
      listDir(path: string, extensions: string[]): Promise<string[]>;
      readFile(path: string): Promise<string>;
      writeFile(path: string, content: string): Promise<unknown>;
      copyFile(sourcePath: string, destinationPath: string): Promise<unknown>;
    };
    taskApi: {
      readTask(task: string): Promise<string>;
      writeTask(task: TaskFile): Promise<unknown>;
      listTasks(): Promise<string[]>;
      renameTask(source: string, destination: string): Promise<unknown>;
      deleteTask(task: string): Promise<unknown>;
      onAdded(callback: (filename: string) => void): void;
      onRemoved(callback: (filename: string) => void): void;
    };
    log: {
      init(options: ExperimentOptions): Promise<void>;
      next(): Promise<void>;
      stop(): Promise<void>;
      initPlot(): Promise<Array<string>>;
      onHeaders(callback: (values: Array<string>) => void): void;
      onData(callback: (values: Array<number>) => void): void;
    };
    library: {
      list_experiments(): Promise<Array<ExperimentDir>>;
      list_tasks(experiment: string): Promise<string[]>;
      read_csv(csv_path: string): Promise<CSVOptions>;
    };
    serial: {
      listDevices(): Promise<string[]>;
      isConnected(): Promise<boolean>;
      connect(port: string): void;
      disconnect(): void;
      onOpen(callback: () => void): void;
      onClose(callback: () => void): void;
      onError(callback: (error: string) => void): void;
    };
    ws_api: {
      getHWInfo(callback?: (num_motors: number, log_list: Array<string>) => void): void;
      getFWVersion(callback?: (version: string) => void): void;
      getControllersList(callback?: (ctrl_list: Array<string>) => void): void;
      getReferencesList(callback?: (refs_list: Array<string>) => void): void;
      getControllerParams(
        ctrl_id: string,
        callback?: (controllerName: string, params_list: Array<string>) => void
      ): void;
      getReferenceParams(ref_id: string, callback?: (referenceName: string, params_list: Array<string>) => void): void;
      getControllerLogs(ctrl_id: string, callback?: (controllerName: string, logs_list: Array<string>) => void): void;
      getReady(callback?: (ready: boolean) => void): void;

      setController(id_ctrl: string, motor: number, parameters: number[]): void;
      setHWLoggables(loggablesHW: string[]): void;
      setControllerLoggables(motorNumber: number, loggablesControllers: string[]): void;
      setReference(id_ref: string, motor: number, parameters: number[]): void;
      startLoop(frequency: number, duration: number, onEndCallback?: () => void): void;
      standby(): void;
    };
  }
}

contextBridge.exposeInMainWorld('myWindowAPI', {
  minimize() {
    BrowserWindow.getFocusedWindow()?.minimize();
  },

  toggleMaximize() {
    const win = BrowserWindow.getFocusedWindow();

    if (win?.isMaximized()) {
      win.unmaximize();
    } else {
      win?.maximize();
    }
  },

  close() {
    BrowserWindow.getFocusedWindow()?.close();
  },
});

contextBridge.exposeInMainWorld('openWindow', {
  plot: () => {
    return ipcRenderer.invoke('open:plot');
  },
});

contextBridge.exposeInMainWorld('fs_api', {
  listDir: (path: string, extensions: Array<string>): Promise<Array<string>> => {
    return ipcRenderer.invoke('listDir', { path: path, extensions: extensions }) as Promise<Array<string>>;
  },
  readFile: (path: string) => {
    return ipcRenderer.invoke('readFile', path);
  },
  writeFile: (path: string, content: string) => {
    return ipcRenderer.invoke('writeFile', { path: path, content: content }) as Promise<unknown>;
  },
  copyFile: (sourcePath: string, destinationPath: string) => {
    return ipcRenderer.invoke('copyFile', { source: sourcePath, destination: destinationPath });
  },
});

contextBridge.exposeInMainWorld('taskApi', {
  writeTask: (task: TaskFile) => {
    return ipcRenderer.invoke('task:write', task);
  },
  listTasks: () => {
    return ipcRenderer.invoke('task:list');
  },
  readTask: (path: string) => {
    return ipcRenderer.invoke('task:read', path);
  },
  renameTask: (source: string, destination: string) => {
    return ipcRenderer.invoke('task:rename', source, destination);
  },
  deleteTask: (path: string) => {
    return ipcRenderer.invoke('task:delete', path);
  },
  onAdded: (callback: (filename: string) => void) => {
    ipcRenderer.on('task:added', (event, filename) => {
      callback(filename);
    });
  },
  onRemoved: (callback: (filename: string) => void) => {
    ipcRenderer.on('task:removed', (event, filename) => {
      callback(filename);
    });
  },
});

contextBridge.exposeInMainWorld('serial', {
  listDevices: () => {
    return ipcRenderer.invoke('serial:list_devices');
  },
  isConnected: () => {
    return ipcRenderer.invoke('serial:is_connected');
  },
  connect: (port: string) => {
    return ipcRenderer.invoke('serial:connect', port);
  },
  disconnect: () => {
    return ipcRenderer.invoke('serial:disconnect');
  },
  onOpen: (callback: () => void) => {
    ipcRenderer.removeAllListeners('serial:open');
    ipcRenderer.on('serial:open', (event) => {
      callback();
    });
  },
  onClose: (callback: () => void) => {
    ipcRenderer.removeAllListeners('serial:close');
    ipcRenderer.on('serial:close', (event) => {
      callback();
    });
  },
  onError: (callback: (error: string) => void) => {
    ipcRenderer.on('serial:rcv_error', (event, error: string) => {
      callback(error);
    });
  },
});

contextBridge.exposeInMainWorld('log', {
  init: (options: ExperimentOptions) => {
    return ipcRenderer.invoke('log:start', options);
  },
  next: () => {
    return ipcRenderer.invoke('log:next');
  },
  stop: () => {
    return ipcRenderer.invoke('log:stop');
  },
  initPlot: () => {
    return ipcRenderer.invoke('plot:req_headers') as Promise<Array<string>>;
  },
  onHeaders: (callback: (values: Array<string>) => void) => {
    ipcRenderer.removeAllListeners('plot:headers');
    ipcRenderer.on('plot:headers', (event, values: Array<string>) => {
      callback(values);
    });
  },
  onData: (callback: (values: Array<number>) => void) => {
    ipcRenderer.removeAllListeners('plot:data');
    ipcRenderer.on('plot:data', (event, values: Array<number>) => {
      callback(values);
    });
  },
});

contextBridge.exposeInMainWorld('library', {
  list_experiments: () => {
    return ipcRenderer.invoke('library:list_experiments');
  },
  list_tasks: (experimentName: string) => {
    return ipcRenderer.invoke('library:list_tasks', experimentName);
  },
  read_csv: (csv_path: string) => {
    return ipcRenderer.invoke('library:read_csv', csv_path);
  },
});

// WEBSOCKET ------------------------------------------------------------------

contextBridge.exposeInMainWorld('ws_api', {
  getHWInfo: (callback?: (num_motors: number, log_list: Array<string>) => void) => {
    if (callback)
      ipcRenderer.once('ws:rcv_hw_info', (event, num_motors, log_list) => {
        callback(num_motors, log_list);
      });
    return ipcRenderer.invoke('ws:get_hw_info');
  },
  getFWVersion: (callback?: (version: string) => void) => {
    if (callback)
      ipcRenderer.once('ws:rcv_fw_version', (event, version) => {
        callback(version);
      });
    return ipcRenderer.invoke('ws:get_fw_version');
  },
  getControllersList: (callback?: (controllerList: string[]) => void) => {
    if (callback)
      ipcRenderer.once('ws:rcv_ctrl_list', (event, controllerList) => {
        callback(controllerList);
      });
    return ipcRenderer.invoke('ws:get_ctrls_list');
  },
  getReferencesList: (callback?: (refs_list: Array<string>) => void) => {
    if (callback)
      ipcRenderer.once('ws:rcv_refs_list', (event, refs_list) => {
        callback(refs_list);
      });
    return ipcRenderer.invoke('ws:get_refs_list');
  },
  getControllerParams: (ctrl_id: string, callback?: (controllerName: string, params_list: Array<string>) => void) => {
    if (callback)
      ipcRenderer.once('ws:rcv_ctrl_params_' + ctrl_id, (event, controllerName, params_list) => {
        callback(controllerName, params_list);
      });
    return ipcRenderer.invoke('ws:get_ctrl_params', ctrl_id);
  },
  getReferenceParams: (ref_id: string, callback?: (referenceName: string, params_list: Array<string>) => void) => {
    if (callback)
      ipcRenderer.once('ws:rcv_ref_params_' + ref_id, (event, referenceName, params_list) => {
        callback(referenceName, params_list);
      });
    return ipcRenderer.invoke('ws:get_ref_params', ref_id);
  },
  getControllerLogs: (ctrl_id: string, callback?: (controllerName: string, logs_list: Array<string>) => void) => {
    if (callback)
      ipcRenderer.once('ws:rcv_ctrl_logs_' + ctrl_id, (event, controllerName, logs_list) => {
        callback(controllerName, logs_list);
      });
    return ipcRenderer.invoke('ws:get_ctrl_logs', ctrl_id);
  },
  getReady: (callback?: (ready: boolean) => void) => {
    if (callback)
      ipcRenderer.once('ws:rcv_ready', (event, ready: boolean) => {
        callback(ready);
      });
    return ipcRenderer.invoke('ws:ready');
  },

  setController: (id_ctrl: string, motor: number, parameters: number[]) => {
    return ipcRenderer.invoke('ws:set_controller', id_ctrl, motor, parameters);
  },
  setHWLoggables: (loggablesHW: string[]) => {
    return ipcRenderer.invoke('ws:set_hw_loggables', loggablesHW);
  },
  setControllerLoggables: (motorNumber: number, loggablesControllers: string[]) => {
    return ipcRenderer.invoke('ws:set_ctrl_loggables', motorNumber, loggablesControllers);
  },
  setReference: (id_ref: string, motor: number, parameters: number[]) => {
    return ipcRenderer.invoke('ws:set_reference', id_ref, motor, parameters);
  },
  startLoop: (frequency: number, duration: number, onEndCallback?: () => void) => {
    ipcRenderer.removeAllListeners('serial:rcv_end_exp');
    if (onEndCallback) {
      ipcRenderer.once('serial:rcv_end_exp', () => {
        onEndCallback();
      });
    }
    return ipcRenderer.invoke('ws:start_loop', frequency, duration);
  },
  standby: () => {
    return ipcRenderer.invoke('ws:standby');
  },
});

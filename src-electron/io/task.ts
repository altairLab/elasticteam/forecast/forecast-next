import { TaskFile } from 'src/models/files'
import * as fs from 'fs';
import * as path from 'path';
import { listFiles, readFile, writeFile, renameFile, getPathFromString, deleteFile } from './fsutils'

export const listTasks = () => {
  return listFiles(getPathFromString('task'), ['.task']).then((list: string[]) => {
    return list.map(entry => path.parse(entry).name)
  });
}

export const saveTask = (task: TaskFile) => {
  fs.mkdir(getPathFromString('task'), {recursive: true}, (err) => {
    if (err)
      throw err;
  });
  const filePath = path.join(getPathFromString('task'), task.name + '.task')
  return writeFile(filePath, JSON.stringify(task));
}

export const readTask = (task: string) => {
  const filePath = path.join(getPathFromString('task'), task + '.task');
  return readFile(filePath);
}

export const renameTask = (source: string, destination: string) => {
  const sourcePath = path.join(getPathFromString('task'), source + '.task');
  const destinationPath = path.join(getPathFromString('task'), destination + '.task');
  return renameFile(sourcePath, destinationPath);
}

export const deleteTask = (task: string) => {
  const filePath = path.join(getPathFromString('task'), task + '.task');
  return deleteFile(filePath);
}
import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/', component: () => import('pages/Index.vue') },
      { path: '/experiment', component: () => import('pages/Experiment.vue') },
      { path: '/taskEditor', component: () => import('pages/Task.vue'), props: {editorMode: true} },
      { path: '/experiment/task', component: () => import('pages/Task.vue'), props: {editorMode: false} },
      { path: '/TestWSPage', component: () => import('pages/TestWSPage.vue') },
      { path: '/library', component: () => import('pages/Library.vue')},
      { path: '/classic', component: () => import('pages/Classic.vue')}
    ],
  },

  {
    path: '/plot',
    component: () => import('layouts/SecondaryLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Plot.vue') }
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue'),
  },
];

export default routes;

import { Scene } from './workflow';

export interface TaskFile {
  name: string;
  version: string;
  scene: Scene;
}

import { RawTask, RefinedTask, Scene } from './workflow';

export interface Experiment {
  blocks: Array<ExperimentBlock>;
}

export interface ExperimentBlock {
  id: string;
  rawTemplate: Scene | null;
  template: RefinedTask | null;
  loopMap: TaskLoopMap | null;
  queue: Array<RefinedTask>;
  currentIteration: number;
  currentTime: number;
}

export interface ExperimentBlockUpdate {
  id: string;
  rawTemplate?: Scene | null;
  template?: RefinedTask | null;
  loopMap?: TaskLoopMap | null;
  queue?: Array<RefinedTask>;
  currentIteration?: number;
  currentTime?: number;
}

export interface ExperimentOptions {
  selected: string;
  isRunning: boolean;
}

export interface CurrentlyRunning {
  blockIndex: number;
  queueIndex: number;
}
export interface TaskLoopMap {
  testbed: {
    testbed: {
      name: string;
      parameters: {
        [name: string]: string;
      };
    };
  };
  controllers: {
    [name: string]: {
      name: string;
      parameters: {
        [name: string]: string;
      };
    };
  };
  references: {
    [name: string]: {
      name: string;
      parameters: {
        [name: string]: string;
      };
    };
  };
}

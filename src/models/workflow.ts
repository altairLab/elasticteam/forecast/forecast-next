export interface Node {
  name: string;
  family: string;
  inputs: Array<string>;
  outputs: Array<string>;
  loggable: Array<string>;
  data: Array<string>;
}

export interface NodeUpdate {
  name: string;
  family: string;
  inputs?: Array<string>;
  outputs?: Array<string>;
  loggable?: Array<string>;
  data?: Array<string>;
}

export interface Point2D {
  x: number;
  y: number;
}

export interface RefinedLink {
  id: string;
  start: Point2D;
  end: Point2D;
  fromBlock: string;
  toBlock: string;
  fromSlot: number;
  toSlot: number;
}

export interface RawBlock {
  id: string;
  position: Point2D;
  family: string;
}

export interface RefinedBlock extends RawBlock {
  name: string;
  inputs: Array<Slot>;
  outputs: Array<Slot>;
  loggable: Loggable;
  data: Data;
}

export interface Slot {
  name: string;
  active: string;
}

export interface Loggable {
  [name: string]: boolean;
}
export interface Data {
  [name: string]: number | null;
}

export interface Scene {
  nodes: Array<Node>;
  blocks: Array<RefinedBlock>;
  links: Array<RefinedLink>;
  container: {
    center: Point2D;
    scale: number;
  };
}

export interface BlockOptions {
  center: Point2D;
  selected: string;
}

export interface RawTask {
  testbed: RefinedBlock | null;
  controllers: {
    [name: string]: RefinedBlock | null;
  };
  references: {
    [name: string]: RefinedBlock | null;
  };
}

export interface RefinedTask {
  testbed: {
    parameters: {
      [name: string]: number;
    };
    loggable: Array<string>;
  };
  controllers: {
    [name: string]: {
      name: string,
      parameters: {
        [name: string]: number;
      };
      loggable: Array<string>;
    };
  };
  references: {
    [name: string]: {
      name: string,
      parameters: {
        [name: string]: number;
      };
    };
  };
}

import { Experiment } from 'src/models/experiment';
import { RefinedTask } from 'src/models/workflow';
export interface ExperimentStateInterface {
  editingBlockID: string;
  currentExperiment: Experiment;
}

function state(): ExperimentStateInterface {
  return {
    currentExperiment: {
      blocks: [],
    },
    editingBlockID: '',
  };
}

export default state;

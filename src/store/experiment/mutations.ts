import { MutationTree } from 'vuex';
import { ExperimentStateInterface } from './state';
import { ExperimentBlock, ExperimentBlockUpdate } from 'src/models/experiment'
import { RefinedTask } from 'src/models/workflow';

const mutation: MutationTree<ExperimentStateInterface> = {
  addBlock: (state: ExperimentStateInterface, block: ExperimentBlock) => {
    state.currentExperiment.blocks.push(block);
  },
  updateBlock: (state: ExperimentStateInterface, updateBlock: ExperimentBlockUpdate) => {
    const blockIndex = state.currentExperiment.blocks.findIndex(block => block.id === updateBlock.id);
    state.currentExperiment.blocks[blockIndex] = { ...state.currentExperiment.blocks[blockIndex], ...updateBlock }
  },
  removeBlock: (state: ExperimentStateInterface, id: string) => {
    state.currentExperiment.blocks = state.currentExperiment.blocks.filter(block => block.id !== id);
  },

  setEditingBlock: (state: ExperimentStateInterface, id: string) => {
    state.editingBlockID = id;
  }
};

export default mutation;

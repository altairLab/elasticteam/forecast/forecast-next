import { boot } from 'quasar/wrappers'
import { Serial } from 'src/components/communication/serial'
import { firstTimeSetup } from 'components/communication/scripts'

// "async" is optional;
// more info on params: https://v2.quasar.dev/quasar-cli/boot-files
export default boot(({store}) => {

  void Serial.isOpen.then(res => store.commit('status/setConnection', res));
  Serial.onOpen(() => {
    store.commit('status/setConnection', true);
    firstTimeSetup();
  });
  Serial.onClose(() => {
    store.commit('status/setConnection', false);
  });
})

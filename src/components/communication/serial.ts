import { errorNotification } from 'components/notifications'

export class Serial {

  static list() {
    return window.serial.listDevices()
  }

  static get isOpen() {
    return window.serial.isConnected();
  }

  static open(path: string) {
    void window.serial.connect(path);
  }

  static close() {
    void window.serial.disconnect();
  }

  static onOpen(callback: () => void) {
    void window.serial.onOpen(callback);
  }

  static onClose(callback: () => void) {
    void window.serial.onClose(callback);
  }

  static onError(callback: (error: string) => void) {
    void window.serial.onError(callback);
  }

  // Interrogations

  static getHWInfo(callback?: (motorsNumber: number, HWLoggables: Array<string>) => void) {
    window.ws_api.getHWInfo(callback);
  }

  static getVERsion(callback?: (version: string) => void) {
    window.ws_api.getFWVersion(callback);
  }

  static getCTRLList(callback?: (controllersList: Array<string>) => void) {
    window.ws_api.getControllersList(callback);
  }

  static getREFList(callback?: (refs_list: Array<string>) => void) {
    window.ws_api.getReferencesList(callback);
  }

  static getCTRLParameters(controllerID: string, callback?: (controllerName: string, parametersList: Array<string>) => void) {
    window.ws_api.getControllerParams(controllerID, callback);
  }

  static getREFParameters(referenceID: string, callback?: (referenceName: string, parametersList: Array<string>) => void) {
    window.ws_api.getReferenceParams(referenceID, callback);
  }

  static getCTRLLoggables(controllerID: string, callback?: (controllerName: string, loggablesList: Array<string>) => void) {
    window.ws_api.getControllerLogs(controllerID, callback);
  }

  static getReady(callback?: (ready: boolean) => void) {
    window.ws_api.getReady(callback);
  }


  // Commands

  static setCTRL(controllerID: string, motorNumber: number, parameters: number[]) {
    void window.ws_api.setController(controllerID, motorNumber, parameters);
  }

  static setREF(referenceID: string, motorNumber: number, parameters: number[]) {
    void window.ws_api.setReference(referenceID, motorNumber, parameters);
  }

  static setHWLoggables(logHW: string[]) {
    void window.ws_api.setHWLoggables(logHW);
  }

  static setCTRLLoggables(motorNumber: number, logController: string[]) {
    void window.ws_api.setControllerLoggables(motorNumber, logController)
  }

  static startLoop(frequency: number, duration: number, onEndCallback?: () => void) {
    void window.ws_api.startLoop(frequency, duration, onEndCallback);
  }

  static standby() {
    void window.ws_api.standby();
  }
}

Serial.onError((error: string) => {
  errorNotification(error);
})

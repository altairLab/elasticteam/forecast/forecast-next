import { RefinedBlock, RefinedLink, Scene, Slot, RawTask } from 'src/models/workflow';

const findTestbedBlock = (blocks: Array<RefinedBlock>) => {
  return blocks.find((block) => block.family === 'testbed');
};

export const scene2task = (scene: Scene): RawTask => {
  const tree = {
    testbed: null,
    controllers: {},
    references: {},
  } as RawTask;

  const testbed = findTestbedBlock(scene.blocks);

  if (!testbed) return tree;
  tree.testbed = testbed;
  const testbedSlots = testbed.inputs.map((slot) => slot.name);
  testbedSlots.forEach((motor) => {
    tree.controllers[motor] = null;
    tree.references[motor] = null;
  });

  testbed.inputs.map((slot: Slot, index: number) => {
    const link = scene.links.find((link: RefinedLink) => link.id === slot.active);
    const controller = scene.blocks.find((block: RefinedBlock) => block.id === link?.fromBlock);
    if (controller) tree.controllers[testbedSlots[index]] = controller;
  });

  Object.values(tree.controllers).forEach((controllerBlock, index: number) => {
    controllerBlock?.inputs.forEach((slot: Slot) => {
      const link = scene.links.find((link: RefinedLink) => link.id === slot.active);
      const referenceBlock = scene.blocks.find((block: RefinedBlock) => block.id === link?.fromBlock);
      if (referenceBlock) tree.references[testbedSlots[index]] = referenceBlock;
    });
  });

  return tree;
};

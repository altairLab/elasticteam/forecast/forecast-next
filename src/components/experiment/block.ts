import { ExperimentBlock } from 'src/models/experiment';
import { RefinedTask } from 'src/models/workflow';
import { refinedToLoopmap, generateTasks } from 'src/components/conversions';
import { v4 as uuidv4 } from 'uuid';

export const createBlock = (template?: RefinedTask): ExperimentBlock => {
  const refinedTemplate = {
    testbed: {
      parameters: {},
      loggable: [],
    },
    controllers: {},
    references: {},
  };

  const block: ExperimentBlock = {
    id: uuidv4(),
    rawTemplate: null,
    template: null,
    loopMap: null, //refinedToLoopmap(template ? template : refinedTemplate),
    queue: [],
    currentIteration: 0,
    currentTime: 0
  };

  if (template) block.template = JSON.parse(JSON.stringify(template)) as RefinedTask;

  return block;
};

export const buildQueue = (block: ExperimentBlock, loopMode: string) => {

  if (block.loopMap && block.template) {
    return generateTasks(block.loopMap, block.template, loopMode);
  } else if (block.template) {
    return [block.template];
  } else {
    return [];
  }
};

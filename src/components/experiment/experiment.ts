import { setTask, startTask, extractLoggables } from 'components/communication/scripts';
import { RefinedTask } from 'src/models/workflow';
import { Serial } from 'components/communication/serial';
import events from 'events';
import { ExperimentOptions } from 'src/models/logs';

export class ExperimentHandler extends events.EventEmitter {
  private _queue: Array<RefinedTask>;
  private _index: number;

  private _tick_timeout!: NodeJS.Timeout;
  private _tick_counter = 0;
  private _run: boolean;

  public onTaskStart: () => void;
  public onTaskEnd: () => void;

  constructor() {
    super();
    this._queue = [];
    this._index = 0;
    this._run = false;
    this.onTaskStart = () => {;};
    this.onTaskEnd = () => {;};
  }

  // Function running while the task is being executed
  private _task_callback(duration: number) {
    this.onTaskStart();
    this._tick_counter = 0;
    this._tick_timeout = setInterval(() => {
      this._tick_counter += 0.2;
      this.emit('tick', this._tick_counter / duration);
    }, 200);
  }

  // Handler for the END_EXPERIMENT packet
  private _task_finished() {
    clearTimeout(this._tick_timeout);
    this.onTaskEnd();
    this.next();
  }

  // Function which checks if all tasks have been run and eventually starts the next one
  private next() {

    // If the user manually stopped or e reached the end of the Q, stop
    if (this._index >= this._queue.length || !this._run) {
      this.emit('finish');
      void window.log.stop();
      return;
    }

    // Retrieve and set the task on the board
    const currentTask = this._queue[this._index];
    setTask(currentTask);

    const taskDuration = currentTask.testbed.parameters['Duration'];
    const taskFrequency = currentTask.testbed.parameters['Frequency'];
    Serial.getReady((ready: boolean) => {
      console.log('Testbed ready: ', ready);
      if (ready) {
        this._task_callback(taskDuration);
        this.emit('next', this._index);
        if (this._index === 0) this.emit('start');
        void window.log.next();
        this._index += 1;
        Serial.startLoop(taskFrequency, taskDuration, () => {
          this._task_finished();
        });
      } else {
        this.stop();
      }
    });
  }

  public load(queue: Array<RefinedTask>) {
    this._queue = queue;
    return queue.length;
  }

  public run(experimentName: string) {

    const options: ExperimentOptions = {
      experimentName: experimentName,
      length: this._queue.length,
      tasks: this._queue.map(task => {
        return {
          headers: extractLoggables(task)
        }
      })

    }
    void window.log.init(options).then(() => {
      this._index = 0;
      this._run = true;
      this.next();
    });
  }

  public stop() {
    this._run = false;
    Serial.standby();
    this.emit('finish');
    void window.log.stop();
  }
}

import { RawTask, RefinedTask } from 'src/models/workflow';
import { TaskLoopMap } from 'src/models/experiment';
import { cartesian } from './cartesian';
import { readFile } from 'app/src-electron/io/fsutils';
import { stringify } from 'querystring';

export const refineTask = (task: RawTask): RefinedTask => {
  const refined_task = {
    testbed: {
      parameters: {},
      loggable: [],
    },
    controllers: {},
    references: {},
  } as RefinedTask;

  if (!task.testbed) return refined_task;

  Object.entries(task.testbed.data).forEach((entry) => {
    const [key, value] = entry;
    if (value !== null) refined_task.testbed.parameters[key] = value;
  });
  Object.entries(task.testbed.loggable).forEach((entry) => {
    const [key, value] = entry;
    if (value) refined_task.testbed.loggable.push(key);
  });

  Object.entries(task.controllers).forEach((entry) => {
    const [key, value] = entry;
    if (value) {
      const temp = {
        name: value.name,
        loggable: [] as string[],
        parameters: {} as Record<string, number>,
      };

      Object.entries(value.data).forEach((parameter) => {
        const [parameterName, parameterValue] = parameter;
        if (parameterValue !== null) temp.parameters[parameterName] = parameterValue;
      });

      Object.entries(value.loggable).forEach((loggable) => {
        const [logName, logValue] = loggable;
        if (logValue) temp.loggable.push(logName);
      });

      refined_task.controllers[key] = temp;
    }
  });

  Object.entries(task.references).forEach((entry) => {
    const [key, value] = entry;
    if (value) {
      const temp = {
        name: value.name,
        parameters: {} as Record<string, number>,
      };

      Object.entries(value.data).forEach((parameter) => {
        const [parameterName, parameterValue] = parameter;
        if (parameterValue !== null) temp.parameters[parameterName] = parameterValue;
      });

      refined_task.references[key] = temp;
    }
  });

  return refined_task;
};

export const refinedToLoopmap = (task: RefinedTask) => {
  const result: TaskLoopMap = {
    testbed: {
      testbed: {
        name: 'testbed',
        parameters: {},
      },
    },
    controllers: {},
    references: {},
  };

  Object.entries(task.testbed.parameters).forEach((entry) => {
    const [key, value] = entry;
    result.testbed.testbed.parameters[key] = value.toString();
  });

  Object.entries(task.controllers).forEach((entry) => {
    const [motorName, controller] = entry;
    result.controllers[motorName] = {
      name: controller.name,
      parameters: {},
    };
    Object.entries(controller.parameters).forEach((parameter) => {
      const [parameterName, parameterValue] = parameter;
      result.controllers[motorName].parameters[parameterName] = parameterValue.toString();
    });
  });

  Object.entries(task.references).forEach((entry) => {
    const [motorName, reference] = entry;
    result.references[motorName] = {
      name: reference.name,
      parameters: {},
    };
    Object.entries(reference.parameters).forEach((parameter) => {
      const [parameterName, parameterValue] = parameter;
      result.references[motorName].parameters[parameterName] = parameterValue.toString();
    });
  });

  return result;
};

export const generateTasks = (loopMap: TaskLoopMap, task: RefinedTask, loopMode: string) => {
  interface temp {
    [name: string]: number[];
  }

  const status: temp = {};

  const template = JSON.parse(JSON.stringify(task)) as RefinedTask;

  Object.entries(loopMap.testbed.testbed.parameters).forEach((entry) => {
    const [parameterName, parameter] = entry;
    if (parameter.split(',').length > 1) {
      status['testbed/testbed/' + parameterName] = parameter.split(',').map((value) => Number.parseFloat(value));
    } else {
      template.testbed.parameters[parameterName] = Number(parameter);
    }
  });

  Object.entries(loopMap.controllers).forEach((entry) => {
    const [motorName, controller] = entry;
    Object.entries(controller.parameters).forEach((parameter) => {
      const [parameterName, parameterValue] = parameter;
      if (parameterValue.split(',').length > 1) {
        status[['controllers', motorName, parameterName].join('/')] = parameterValue
          .split(',')
          .map((value) => Number.parseFloat(value));
      } else {
        template.controllers[motorName].parameters[parameterName] = Number(parameterValue);
      }
    });
  });

  Object.entries(loopMap.references).forEach((entry) => {
    const [motorName, reference] = entry;
    Object.entries(reference.parameters).forEach((parameter) => {
      const [parameterName, parameterValue] = parameter;
      if (parameterValue.split(',').length > 1) {
        status[['references', motorName, parameterName].join('/')] = parameterValue
          .split(',')
          .map((value) => Number.parseFloat(value));
      } else {
        template.references[motorName].parameters[parameterName] = Number(parameterValue);
      }
    });
  });

  if (Object.keys(status).length === 0)
    return [template];

  const tempp: temp = {};


  let length = 0;

  if (loopMode == 'cartesian') {

    Object.keys(status).forEach((key) => {
      tempp[key] = [];
    });
    cartesian(Object.values(status)).map((values: number[]) => {
      Object.keys(tempp).forEach((key: string, index: number) => {
        tempp[key].push(values[index]);
      });
    });
    length = Object.values(tempp)[0].length;

  } else {
    Object.keys(status).forEach((key) => {
      tempp[key] = status[key];
    });
    length = Math.max(...Object.values(tempp).map(array => array.length));
    Object.keys(tempp).forEach(key => {
      const last_value = tempp[key][tempp[key].length - 1];
      while(tempp[key].length < length)
      tempp[key].push(last_value);
    })
  }


  const result = Array<RefinedTask>();

  for (let i = 0; i < length; ++i) {
    const refined = JSON.parse(JSON.stringify(template)) as RefinedTask;
    Object.entries(tempp).forEach((entry) => {
      const [key, values] = entry;
      const [mainKey, motorName, parameterName] = key.split('/');
      switch (mainKey) {
        case 'testbed':
          refined.testbed.parameters[parameterName] = values[i];
          break;
        case 'controllers':
          refined.controllers[motorName].parameters[parameterName] = values[i];
          break;
        case 'references':
          refined.references[motorName].parameters[parameterName] = values[i];
          break;
      }
    });
    result.push(refined);
  }
  return result;
};

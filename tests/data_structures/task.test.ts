import { assert } from 'chai';
import { describe } from 'mocha';

import { RawTask, RefinedTask } from '../../src/models/workflow';
import { refineTask } from '../../src/components/conversions';

describe('RawTask -> RefinedTask', () => {
  it('Manual check', () => {
    const raw: RawTask = {
      testbed: {
        id: '05aac0f8-341a-4e18-8188-287994a8abd6',
        name: 'Testbed',
        position: {
          x: 672,
          y: 227,
        },
        family: 'testbed',
        inputs: [
          {
            name: 'motor',
            active: 'a067fca1-bce1-405b-b199-26d2bb58d2fd',
          },
          {
            name: 'environment',
            active: '709a94cd-9d2f-4c84-bb6d-d8124efd2fd6',
          },
        ],
        outputs: [],
        loggable: {},
        data: {},
      },
      controllers: {
        motor: {
          id: 'bad7ba49-5cdd-4892-8c6f-2a5d19bd19e6',
          name: 'PositionPID',
          position: {
            x: -306,
            y: 58,
          },
          family: 'controller',
          inputs: [
            {
              name: '',
              active: '',
            },
          ],
          outputs: [
            {
              name: '',
              active: '',
            },
          ],
          loggable: {
            output: false,
          },
          data: {
            KP: 0,
            KI: 0,
            KD: 0,
          },
        },
        environment: {
          id: '7000398e-d49c-4198-9585-d25b4185a9c2',
          name: 'Impedance Control',
          position: {
            x: 190,
            y: 431,
          },
          family: 'controller',
          inputs: [
            {
              name: 'ref',
              active: '1a62dc51-4e6e-4ae9-b6c4-560f312ecc97',
            },
          ],
          outputs: [
            {
              name: 'output',
              active: '',
            },
          ],
          loggable: {
            output: false,
          },
          data: {
            J: 0,
          },
        },
      },
      references: {
        motor: {
          id: 'd10281c3-18ae-4275-93dc-616330c5462a',
          name: 'Step',
          position: {
            x: 56,
            y: 98,
          },
          family: 'reference',
          inputs: [],
          outputs: [
            {
              name: 'output',
              active: '',
            },
          ],
          loggable: {},
          data: {},
        },
        environment: {
          id: 'bfbd4451-d023-4019-816a-b7042bd11e1a',
          name: 'Step',
          position: {
            x: -261,
            y: 218,
          },
          family: 'reference',
          inputs: [],
          outputs: [
            {
              name: '',
              active: '',
            },
          ],
          loggable: {},
          data: {},
        },
      },
    };

    const refined: RefinedTask = {
      testbed: {
        loggable: [],
        parameters: {},
      },
      controllers: {
        motor: {
          name: 'PositionPID',
          loggable: [],
          parameters: {
            KP: 0,
            KI: 0,
            KD: 0,
          },
        },
        environment: {
          name: 'Impedance Control',
          loggable: [],
          parameters: {
            J: 0,
          },
        },
      },
      references: {
        motor: {
          name: 'Step',
          parameters: {},
        },
        environment: {
          name: 'Step',
          parameters: {},
        },
      },
    };

    assert.deepEqual(refineTask(raw), refined);
  });
});
